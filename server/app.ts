import express from 'express';
import http from 'http';
import { router as usersRoutes } from './api/users-api';



const app = express();
const port = 5000;

app.use(express.json());

const server = http.createServer(app);
server.listen(port);


app.use('/users', usersRoutes );

module.exports = app;