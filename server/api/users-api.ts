import express from 'express';
import { con } from '../config/database';
import { TABLES } from '../config/db-tables.constants';
import { NewUserDTO, NewUserExpectedBody } from '../models/new-user.model';
import { Utils } from '../scripts/utils';
import { genSalt, hash }  from 'bcrypt';
import { User } from '../services/User.service';

export const router = express.Router();

// Register new user
router.post('/', async (req, res) => {
    const reqBody: NewUserExpectedBody = req.body;
    
    // hashing of user password with salt of 10 characters
    const hashSalt = await genSalt(10);
    const hashedPassword = await hash(reqBody.password, hashSalt);

    const newUserBody = new NewUserDTO(reqBody, hashedPassword, hashSalt);

    try {
        await User.insert(newUserBody);
        res.status(201).send({message: 'created'});

    } catch (err) {
        res.status(409).send('User already exist')
    }

});

