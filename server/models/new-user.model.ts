export interface NewUserExpectedBody {
    username: string;
    password: string;
    organizationCode: number | null;
    name: string;
    lastname: string;
}


export class NewUserDTO{
    username: string;
    hashed_password: string;
    fk_organization: number | null;
    hash_salt: string;
    name: string;
    lastname: string;

    constructor(newUserBodyUnsecure: NewUserExpectedBody, hashedPassword: string, hashSalt: string) {
        this.name = newUserBodyUnsecure.name;
        this.lastname = newUserBodyUnsecure.lastname;
        this.username = newUserBodyUnsecure.username;
        this.hash_salt = hashSalt;
        this.hashed_password = hashedPassword;
        this.fk_organization = newUserBodyUnsecure.organizationCode ? newUserBodyUnsecure.organizationCode : null;
    }

}