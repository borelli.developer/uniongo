import { con } from "../config/database";
import { TABLES } from "../config/db-tables.constants";
import { NewUserDTO } from "../models/new-user.model";
import { Utils } from "../scripts/utils";

export class User {

    static async insert(newUserDTO: NewUserDTO) {
        return new Promise(async (resolve, reject) => {
            try {
                // Check if user already exist
                const selQueryRes : any[] = await User.findByUsername(newUserDTO.username);
                
                if(!selQueryRes.length) {
                    // No user found with same username. Proceeding with insert into DB.
                    const insertQuery = Utils.makeInsertQuery(TABLES.USERS);
                    con.query(insertQuery, newUserDTO, (err, result, fields) => {
                        if(err) {
                            reject(err);
                        }

                        console.log("NUOVO UTENTE INSERITO CON SUCCESSO")
                        resolve(true);
                    });
                } else {
                    console.log("UTENTE GIA ESISTENTE")
                    reject();
                }
            } catch (err) {
                console.error(err);
                reject()
            }
        })
    
    }

    static findByUsername(username: string) : Promise<any []>{
        return new Promise((resolve, reject) => {
            const selectQuery = "SELECT * FROM t_users WHERE username = '" + username + "' LIMIT 1;";
            con.query(selectQuery, username, (err, results: any []) => {
                if(err) {
                    reject(err);
                } 

                resolve(results);
                
            })
        })

    }

}