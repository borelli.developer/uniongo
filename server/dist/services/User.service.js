"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const database_1 = require("../config/database");
const db_tables_constants_1 = require("../config/db-tables.constants");
const utils_1 = require("../scripts/utils");
class User {
    static async insert(newUserDTO) {
        return new Promise(async (resolve, reject) => {
            try {
                // Check if user already exist
                const selQueryRes = await User.findByUsername(newUserDTO.username);
                if (!selQueryRes.length) {
                    // No user found with same username. Proceeding with insert into DB.
                    const insertQuery = utils_1.Utils.makeInsertQuery(db_tables_constants_1.TABLES.USERS);
                    database_1.con.query(insertQuery, newUserDTO, (err, result, fields) => {
                        if (err) {
                            reject(err);
                        }
                        console.log("NUOVO UTENTE INSERITO CON SUCCESSO");
                        resolve(true);
                    });
                }
                else {
                    console.log("UTENTE GIA ESISTENTE");
                    reject();
                }
            }
            catch (err) {
                console.error(err);
                reject();
            }
        });
    }
    static findByUsername(username) {
        return new Promise((resolve, reject) => {
            const selectQuery = "SELECT * FROM t_users WHERE username = '" + username + "' LIMIT 1;";
            database_1.con.query(selectQuery, username, (err, results) => {
                if (err) {
                    reject(err);
                }
                resolve(results);
            });
        });
    }
}
exports.User = User;
//# sourceMappingURL=User.service.js.map