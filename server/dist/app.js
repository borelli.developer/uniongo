"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const users_api_1 = require("./api/users-api");
const app = express_1.default();
const port = 5000;
app.use(express_1.default.json());
const server = http_1.default.createServer(app);
server.listen(port);
app.use('/users', users_api_1.router);
module.exports = app;
//# sourceMappingURL=app.js.map