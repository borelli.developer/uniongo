"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewUserDTO = void 0;
class NewUserDTO {
    constructor(newUserBodyUnsecure, hashedPassword, hashSalt) {
        this.name = newUserBodyUnsecure.name;
        this.lastname = newUserBodyUnsecure.lastname;
        this.username = newUserBodyUnsecure.username;
        this.hash_salt = hashSalt;
        this.hashed_password = hashedPassword;
        this.fk_organization = newUserBodyUnsecure.organizationCode ? newUserBodyUnsecure.organizationCode : null;
    }
}
exports.NewUserDTO = NewUserDTO;
//# sourceMappingURL=new-user.model.js.map