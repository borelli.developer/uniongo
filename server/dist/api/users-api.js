"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const express_1 = __importDefault(require("express"));
const new_user_model_1 = require("../models/new-user.model");
const bcrypt_1 = require("bcrypt");
const User_service_1 = require("../services/User.service");
exports.router = express_1.default.Router();
// Creazione nuovo user
exports.router.post('/', async (req, res) => {
    const reqBody = req.body;
    // hashing della password con salt di 10 caratteri
    const hashSalt = await bcrypt_1.genSalt(10);
    const hashedPassword = await bcrypt_1.hash(reqBody.password, hashSalt);
    const newUserBody = new new_user_model_1.NewUserDTO(reqBody, hashedPassword, hashSalt);
    try {
        await User_service_1.User.insert(newUserBody);
        res.status(201).send({ message: 'created' });
    }
    catch (err) {
        res.status(409).send({ message: 'User already exist' });
    }
});
//# sourceMappingURL=users-api.js.map